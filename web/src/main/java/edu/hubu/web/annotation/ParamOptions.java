package edu.hubu.web.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author moonlan
 * date 2020/11/22 23:01
 */
@SuppressWarnings(value = "unused")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ParamOptions {
    boolean nullable() default false;

    String defaultValue();
}
