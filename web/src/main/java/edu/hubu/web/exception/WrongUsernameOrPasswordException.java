package edu.hubu.web.exception;

/**
 * @author moonlan
 * @date 2020/10/8 15:29
 */
public class WrongUsernameOrPasswordException extends RuntimeException {
    public WrongUsernameOrPasswordException() {
    }

    public WrongUsernameOrPasswordException(String message) {
        super(message);
    }

    public WrongUsernameOrPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongUsernameOrPasswordException(Throwable cause) {
        super(cause);
    }

    public WrongUsernameOrPasswordException(String message, Throwable cause, boolean enableSuppression,
                                            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
