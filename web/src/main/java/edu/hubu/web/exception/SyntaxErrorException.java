package edu.hubu.web.exception;

/**
 * @author moonlan
 * @date 2020/10/8 15:13
 * 400
 */
public class SyntaxErrorException extends RuntimeException {
    public SyntaxErrorException() {
    }

    public SyntaxErrorException(String message) {
        super(message);
    }

    public SyntaxErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxErrorException(Throwable cause) {
        super(cause);
    }

    public SyntaxErrorException(String message, Throwable cause, boolean enableSuppression,
                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
