package edu.hubu.web.exception;

import org.springframework.web.HttpRequestMethodNotSupportedException;

/**
 * @author moonlan
 * @date 2020/10/8 15:27
 */
public class MethodNotValidException extends HttpRequestMethodNotSupportedException {

    public MethodNotValidException(String message) {
        super(message);
    }

}
