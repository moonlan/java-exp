package edu.hubu.web.exception;

import org.springframework.security.access.AccessDeniedException;

/**
 * @author moonlan
 * @date 2020/10/8 15:26
 */
public class PermissionDeniedException extends AccessDeniedException {
    public PermissionDeniedException(String msg) {
        super(msg);
    }

    public PermissionDeniedException(String msg, Throwable t) {
        super(msg, t);
    }
}
