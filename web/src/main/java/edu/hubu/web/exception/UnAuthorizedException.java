package edu.hubu.web.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author moonlan
 * date 2020/10/8 15:24
 */
public class UnAuthorizedException extends AuthenticationException {
    public UnAuthorizedException(String msg, Throwable t) {
        super(msg, t);
    }

    public UnAuthorizedException(String msg) {
        super(msg);
    }
}
