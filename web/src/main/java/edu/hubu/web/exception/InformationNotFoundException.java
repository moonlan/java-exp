package edu.hubu.web.exception;

/**
 * @author moonlan
 * @date 2020/10/8 15:27
 */
public class InformationNotFoundException extends RuntimeException {
    public InformationNotFoundException() {
    }

    public InformationNotFoundException(String message) {
        super(message);
    }

    public InformationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public InformationNotFoundException(Throwable cause) {
        super(cause);
    }

    public InformationNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
