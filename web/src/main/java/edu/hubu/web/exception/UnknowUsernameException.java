package edu.hubu.web.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author moonlan
 * @date 2020/10/8 15:28
 */
public class UnknowUsernameException extends UsernameNotFoundException {

    public UnknowUsernameException(String message) {
        super(message);
    }

    public UnknowUsernameException(String message, Throwable cause) {
        super(message, cause);
    }

}
