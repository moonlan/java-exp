package edu.hubu.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * @author moonlan
 * @date 2020/10/20 下午5:12
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

//    private final AuthenticationProvider provider;
//    private final UserService userService;
//    private final AuthenticationManager authManager;
//    private final UserDetailsService userDetailsService;
//
//    public SpringSecurityConfig(AuthenticationProvider provider, UserService userService, AuthenticationManager authManager, @Qualifier("userService") UserDetailsService userDetailsService) {
//        this.provider = provider;
//        this.userService = userService;
//        this.authManager = authManager;
//        this.userDetailsService = userDetailsService;
//    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/user/code_paint", "/user/login", "/announcement/search", "/announcement/search_like", "/announcement/count", "/course/search", "/course/search_like", "/course/count");
        web.ignoring().antMatchers("/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/**").authenticated();
//        http.addFilter(new JwtAuthenticationFilter(authManager, userDetailsService, userService));
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(provider);
//    }
}
