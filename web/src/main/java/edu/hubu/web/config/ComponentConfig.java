package edu.hubu.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.dao.impl.*;
import edu.hubu.web.model.*;
import edu.hubu.web.service.DefaultService;
import edu.hubu.web.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.EntityManager;

/**
 * @author moonlan
 * date 2020/10/23 上午11:46
 */
@Configuration
public class ComponentConfig {

    @Autowired
    private EntityManager em;

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public DefaultService<Role, String> roleService(IRoleDao roleDao) {
        return new DefaultService<>(roleDao, Role.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<User, String> userService(IUserDao userDao,BCryptPasswordEncoder encoder,DefaultService<Role,String>roleService, DefaultService<Verification, String> verificationService, EntityManager em) {
        return new UserServiceImpl(userDao,encoder, roleService,verificationService, em);
    }

    @Bean
    public DefaultService<Buyer, String> buyerService(IBuyerDao buyerDao) {
        return new DefaultService<>(buyerDao, Buyer.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<Comment, String> commentService(ICommentDao commentDao) {
        return new DefaultService<>(commentDao, Comment.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<Commodity, String> commodityService(ICommodityDao commodityDao) {
        return new DefaultService<>(commodityDao, Commodity.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<CommodityOption, String> commodityOptionService(ICommodityOptionDao commodityOptionDao) {
        return new DefaultService<>(commodityOptionDao, CommodityOption.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<ImgUrl, String> imgUrlService(IImgUrlDao imgDao) {
        return new DefaultService<>(imgDao, ImgUrl.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<Location, String> locationService(ILocationDao locationDao) {
        return new DefaultService<>(locationDao, Location.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<Seller, String> sellerService(ISellerDao sellerDao) {
        return new DefaultService<>(sellerDao, Seller.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<ShopOrder, String> shopOrderService(IShopOrderDao shopOrderDao) {
        return new DefaultService<>(shopOrderDao, ShopOrder.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Bean
    public DefaultService<Category, String> categoryService(ICategoryDao categoryDao) {
        return new DefaultService<>(categoryDao, Category.class, em, "isEntityIsDeleted", "isEntityIsForbidden");
    }
    @Bean
    public DefaultService<Verification, String> verificationService(IVerificationDao verificationDao) {
        return new DefaultService<>(verificationDao, Verification.class,em,"verificationIsDeactivated");
    }
}
