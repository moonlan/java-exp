package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.CommodityOption;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 20:58
 */
@SuppressWarnings(value = "unused")
@Repository
public interface ICommodityOptionDao extends IBaseDao<CommodityOption, String> {
}
