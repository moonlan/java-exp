package edu.hubu.web.dao.impl;

import edu.hubu.web.model.User;
import edu.hubu.web.dao.IBaseDao;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 20:54
 */
@SuppressWarnings(value = "unused")
@Repository
public interface IUserDao extends IBaseDao<User, String> {
}
