package edu.hubu.web.dao;

import edu.hubu.web.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author moonlan
 * date 2020/11/22 15:54
 */
@SuppressWarnings(value = "unused")
@NoRepositoryBean
public interface IBaseDao<EntityType extends BaseEntity, IdType extends Serializable> extends JpaRepository<EntityType, IdType>, JpaSpecificationExecutor<EntityType> {
}
