package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.Location;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 20:59
 */
@SuppressWarnings(value = "unused")
@Repository
public interface ILocationDao extends IBaseDao<Location, String> {
}
