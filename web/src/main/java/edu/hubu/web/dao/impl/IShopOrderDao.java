package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.ShopOrder;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 21:00
 */
@SuppressWarnings(value = "unused")
@Repository
public interface IShopOrderDao extends IBaseDao<ShopOrder, String> {
}
