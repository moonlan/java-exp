package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.Category;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/12/1 16:29
 */
@SuppressWarnings(value = "unused")
@Repository
public interface ICategoryDao  extends IBaseDao<Category, String> {
}
