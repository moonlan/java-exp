package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.Commodity;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 20:57
 */
@SuppressWarnings(value = "unused")
@Repository
public interface ICommodityDao extends IBaseDao<Commodity, String> {
}
