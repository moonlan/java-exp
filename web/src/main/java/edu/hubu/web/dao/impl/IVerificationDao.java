package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.Verification;
import org.springframework.stereotype.Repository;


@Repository
public interface IVerificationDao extends IBaseDao<Verification,String> {
}
