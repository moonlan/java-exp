package edu.hubu.web.dao.impl;

import edu.hubu.web.dao.IBaseDao;
import edu.hubu.web.model.Comment;
import org.springframework.stereotype.Repository;

/**
 * @author moonlan
 * date 2020/11/25 20:56
 */
@SuppressWarnings(value = "unused")
@Repository
public interface ICommentDao extends IBaseDao<Comment, String> {
}
