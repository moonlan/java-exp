package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.annotation.FieldIgnore
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.Pattern

/**
 * @author moonlan
 * date 2020/11/25 17:52
 */
@Table(name = "buyer")
@Entity
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Buyer : User() {
    @Id
    @Column(name = "user_id")
    override open var userId: String? = null

    @Column(name = "user_name", nullable = false, unique = true)
    override open var userName: String? = null

    @Column(name = "user_password", nullable = false)
    @FieldIgnore
    override open var userPassword: String? = null

    @Column(name = "user_email", nullable = false, unique = true)
    override open var userEmail: @Email(message = "邮箱格式不正确") String? = null

    @Column(name = "user_token")
    override open var userToken: String? = null

    @Column(name = "user_telephone_number", nullable = false, unique = true)
    override open var userTelephoneNumber: @Pattern(regexp = "^[1][34578][0-9]{9}$", message = "手机号码不正确") String? = null

    @Column(name = "user_create_time")
    @CreatedDate
    override open var userCreateTime: LocalDateTime? = null

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    override open val location: MutableList<Location> = mutableListOf()

    @ManyToMany(targetEntity = Role::class, fetch = FetchType.LAZY, cascade = [CascadeType.REFRESH])
    @JoinTable(
        name = "user_roles",
        joinColumns = [JoinColumn(name = "user_roles_user_id", referencedColumnName = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "user_roles_role_id", referencedColumnName = "role_id")]
    )
    override open val roles: MutableList<Role> = mutableListOf()

    @Column(name = "buyer_rest_money")
    open var restMoney = 0.0

    @Column(name = "entity_is_deleted")
    override var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    override var isEntityIsForbidden = false

    @OneToMany(mappedBy = "buyer")
    @JsonIgnore
    open val orders: MutableList<ShopOrder> = mutableListOf()
}