package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/11/25 17:56
 */
@Table(name = "location")
@Entity
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Location : BaseEntity() {
    @Id
    @Column(name = "location_id")
    open var locationId: String? = null

    @Column(name = "location_content")
    open var locationContent: String? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToOne(targetEntity = User::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "location_user_id", referencedColumnName = "user_id")
    open var user: User? = null

    @OneToMany(mappedBy = "location")
    @JsonIgnore
    open val orders: MutableList<ShopOrder> = mutableListOf()
}