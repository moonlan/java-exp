package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.commons.ObjectUtils
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/11/25 18:39
 */
@Entity
@Table(name = "img_url")
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class ImgUrl : BaseEntity() {
    @Id
    @Column(name = "img_url_id")
    open var imgUrlId: String? = null

    @Column(name = "img_url")
    open var imgUrl: String? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToOne(targetEntity = Commodity::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "img_url_commodity_id", referencedColumnName = "commodity_id")
    open var commodity: Commodity? = null

}