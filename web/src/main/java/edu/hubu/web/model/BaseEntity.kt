package edu.hubu.web.model

import java.util.*
import javax.persistence.MappedSuperclass

/**
 * @author moonlan
 * date 2020/11/25 17:54
 */
@MappedSuperclass
abstract class BaseEntity {
    //    @Column(name = "entity_is_deleted")
//    open var isEntityIsDeleted = false
//
//    @Column(name = "entity_is_forbidden")
//    open var isEntityIsForbidden = false
    fun getUUID(prefix: String, uuid: String): String {
        val year = Calendar.getInstance().get(Calendar.YEAR)
        return "${prefix.toUpperCase()}-${year}-${uuid}"
    }
}