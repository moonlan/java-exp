package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.commons.ObjectUtils
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/12/1 16:15
 */
@Table(name = "category")
@Entity
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Category() : BaseEntity() {
    @Id
    @Column(name = "category_id")
    open var categoryId: String? = null

    @Column(name = "category_name", nullable = false, unique = true)
    open var categoryName: String? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    open val commodities: MutableList<Commodity> = mutableListOf()


}