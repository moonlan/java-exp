package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.commons.ObjectUtils
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/11/25 18:15
 */
@Table(name = "commodity_option")
@Entity
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class CommodityOption : BaseEntity() {
    @Id
    @Column(name = "commodity_option_id")
    open var commodityOptionId: String? = null

    @Column(name = "commodity_option_name")
    open var commodityOptionName: String? = null

    @Column(name = "commodity_option_value")
    open var commodityOptionValue: String? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToOne(targetEntity = Commodity::class, cascade = [CascadeType.REFRESH])
    @JoinColumn(name = "commodity_option_commodity_id", referencedColumnName = "commodity_id")
    open var commodity: Commodity? = null
}