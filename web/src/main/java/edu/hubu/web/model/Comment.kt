package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.DecimalMax

/**
 * @author moonlan
 * date 2020/11/25 18:33
 */
@Entity
@Table(name = "comment")
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Comment : BaseEntity() {
    @Id
    @Column(name = "comment_id")
    open var commentId: String? = null

    @Column(name = "comment_content")
    open var commentContent: String? = null

    @Column(name = "comment_agree_number")
    open var commentAgreeNumber: Int? = null

    @Column(name = "comment_create_time")
    @CreatedDate
    open var commentCreateTime: LocalDateTime? = null

    @Column(name = "comment_is_anonymous")
    open var commentIsAnonymous = false
    open var commentScoreToCommodity: @DecimalMax(value = "10.0", message = "分数最大为10") Double? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToOne(targetEntity = User::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_user_id", referencedColumnName = "user_id")
    open var user: User? = null

    //TODO:回复评论
    //List<Comment> replies
    @ManyToOne(targetEntity = Commodity::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_commodity_id", referencedColumnName = "commodity_id")
    open var commodity: Commodity? = null

}