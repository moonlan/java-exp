package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.commons.ObjectUtils
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.DecimalMax

/**
 * @author moonlan
 * date 2020/11/25 18:13
 */
@Table(name = "commodity")
@Entity
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Commodity : BaseEntity() {
    @Id
    @Column(name = "commodity_id")
    open  var commodityId: String? = null

    @Column(name = "commodity_name")
    open var commodityName: String? = null

    @Column(name = "commodity_description")
    open var commodityDescription: String? = null

    @Column(name = "commodity_added_time")
    @CreatedDate
    open  var commodityAddedTime: LocalDateTime? = null

    @Column(name = "commodity_price")
    open var commodityPrice: Double? = null

    @Column(name = "commodity_discount")
    open var commodityDiscount: @DecimalMax(value = "1.0", message = "折扣最大为1.0") Double? = null

    @Column(name = "commodity_score")
    open var commodityScore: @DecimalMax(value = "10.0", message = "分数最大为10") Double? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @OneToMany(mappedBy = "commodity")
    @JsonIgnore
    open val commodityOptions: List<CommodityOption> = ArrayList()

    @ManyToOne(targetEntity = Seller::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_seller_id", referencedColumnName = "user_id")
    open var seller: Seller? = null

    @OneToMany(mappedBy = "commodity")
    @JsonIgnore
    open val orders: MutableList<ShopOrder> = mutableListOf()

    @OneToMany(mappedBy = "commodity")
    @JsonIgnore
    open val comments: MutableList<Comment> = mutableListOf()

    @ManyToOne(targetEntity = Category::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "commodity_category_id", referencedColumnName = "category_id")
    open var category: Category? = null

    @OneToMany(mappedBy = "commodity")
    @JsonIgnore
    open val urls: MutableList<ImgUrl> = mutableListOf()

}