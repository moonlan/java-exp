package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/11/25 17:40
 */
@Table(name = "role")
@Entity
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class Role : BaseEntity() {
    @Id
    @Column(name = "role_id")
    open var roleId: String? = null

    @Column(name = "role_name")
    open var roleName: String? = null

    @Column(name = "role_create_time")
    @CreatedDate
    open var roleCreateTime: LocalDateTime? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    open var users: MutableList<User> = mutableListOf()

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    open var sellers: MutableList<Seller> = mutableListOf()

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    open var buyers: MutableList<Buyer> = mutableListOf()

}