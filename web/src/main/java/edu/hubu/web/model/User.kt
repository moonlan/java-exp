package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.annotation.FieldIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.Pattern

/**
 * @author moonlan
 * date 2020/11/25 17:29
 */
@Table(name = "user")
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class User : BaseEntity() {
    @Id
    @Column(name = "user_id")
    open var userId: String? = null

    @Column(name = "user_name", nullable = false, unique = true)
    open var userName: String? = null

    @Column(name = "user_password", nullable = false)
    @FieldIgnore
    open var userPassword: String? = null

    @Column(name = "user_email", nullable = false, unique = true)
    @FieldIgnore
    open var userEmail: @Email(message = "邮箱格式不正确") String? = null

    @Column(name = "user_token")
    @FieldIgnore
    open var userToken: String? = null

    @Column(name = "user_telephone_number", nullable = false, unique = true)
    @FieldIgnore
    open var userTelephoneNumber: @Pattern(regexp = "^[1][34578][0-9]{9}$", message = "手机号码不正确") String? = null

    @Column(name = "user_create_time")
    @CreatedDate
    open var userCreateTime: LocalDateTime? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    open val location: MutableList<Location> = mutableListOf()

    @ManyToMany(targetEntity = Role::class, fetch = FetchType.LAZY, cascade = [CascadeType.REFRESH])
    @JoinTable(
        name = "user_roles",
        joinColumns = [JoinColumn(name = "user_roles_user_id", referencedColumnName = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "user_roles_role_id", referencedColumnName = "role_id")]
    )
    open val roles: MutableList<Role> = mutableListOf()

//    @OneToOne(mappedBy = "user",fetch = FetchType.LAZY)
//    @JsonIgnore
//    open var code:Verification? = null
}