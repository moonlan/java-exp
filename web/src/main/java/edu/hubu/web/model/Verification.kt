package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Table(name = "verification")
@Entity
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
class Verification: BaseEntity() {

    @Id
    @Column(name = "verification_id")
    open  var verificationId: String? = null

    @Column(name = "verification_code")
    open var verificationCode: String? = null

    @Column(name = "verification_email")
    open var verificationEmail: String? = null

    @Column(name = "verification_time_out_stamp")
    open var verificationTimeOutStamp: Date?=null

    @Column(name = "verification_is_deactivated")
    open var verificationIsDeactivated: Boolean=false

//
//    @OneToOne(targetEntity = User::class,cascade = [CascadeType.REFRESH],fetch = FetchType.LAZY)
//    @JoinColumn(name = "verification_user_id",referencedColumnName = "user_id")
//    open var user: User? = null
}