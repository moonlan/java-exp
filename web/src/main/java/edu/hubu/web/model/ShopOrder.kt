package edu.hubu.web.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import edu.hubu.web.commons.ObjectUtils
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * @author moonlan
 * date 2020/11/25 20:26
 */
@Table(name = "shop_order")
@Entity
@EntityListeners(AuditingEntityListener::class)
@JsonIgnoreProperties(value = ["hibernateLazyInitializer", "handler", "fieldHandler"])
open class ShopOrder : BaseEntity() {
    @Id
    @Column(name = "order_id")
    open var orderId: String? = null

    @Column(name = "order_single_money")
    open var orderMoney: Double? = null

    @Column(name = "order_number")
    open var orderNumber: Long? = null

    @Column(name = "order_total_money")
    open var orderTotalMoney: Double? = null

    @Column(name = "order_discount")
    open var orderDiscount: Double? = null

    @Column(name = "order_create_time")
    @CreatedDate
    open var orderCreateTime: LocalDateTime? = null

    @Column(name = "order_is_committed")
    open var orderIsCommitted: Boolean? = null

    @Column(name = "order_is_finished")
    open var orderIsFinished: Boolean? = null

    @Column(name = "entity_is_deleted")
    open var isEntityIsDeleted = false

    @Column(name = "entity_is_forbidden")
    open var isEntityIsForbidden = false

    @ManyToOne(targetEntity = Buyer::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "order_buyer_id", referencedColumnName = "user_id")
    open var buyer: Buyer? = null

    @ManyToOne(targetEntity = Commodity::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "order_commodity_id", referencedColumnName = "commodity_id")
    open var commodity: Commodity? = null

    @ManyToOne(targetEntity = Location::class, cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "order_location_id", referencedColumnName = "location_id")
    open var location: Location? = null

}