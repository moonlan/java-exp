package edu.hubu.web.controller.impl;


import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.ShopOrder;
import edu.hubu.web.service.DefaultService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/shopOrder")
@SuppressWarnings("unused")
public class ShopOrderController extends CurdController<ShopOrder> {

    private final DefaultService<ShopOrder,String> shopOrderService;

    public ShopOrderController(DefaultService<ShopOrder, String> shopOrderService) {
        this.shopOrderService = shopOrderService;
    }

    @Override
    protected ShopOrder register() {
        return new ShopOrder();
    }

    @Override
    public ResultsJson findOneById(@PathVariable("id") String id,@RequestHeader String token) {
        ShopOrder shopOrderById = shopOrderService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(shopOrderService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(shopOrderService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,shopOrderById);

    }

    @Override
    public ResultsJson findByLikePage(@RequestParam(required = false)int currentPage,
                                      @RequestParam(required = false) int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<ShopOrder> shopOrderPageData = shopOrderService.getEntitiesPaged(currentPage, size, t, shopOrder -> ObjectUtils.Companion.reflectCheckFieldValue(shopOrder, Objects.requireNonNull(shopOrderService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(shopOrder, Objects.requireNonNull(shopOrderService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, shopOrderPageData.getTotalSize(),currentPage,shopOrderPageData);
    }

    @Override
    public ResultsJson findAllPage(@RequestParam(required = false) int currentPage,
                                   @RequestParam(required = false) int size,
                                   @RequestHeader String token) {
        PageData<ShopOrder> shopOrderAll = shopOrderService.getEntitiesPaged(currentPage, size, null, shopOrder -> ObjectUtils.Companion.reflectCheckFieldValue(shopOrder, Objects.requireNonNull(shopOrderService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(shopOrder, Objects.requireNonNull(shopOrderService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,shopOrderAll.getTotalSize(),currentPage,shopOrderAll);
    }


    @Override
    public ResultsJson updateById(@PathVariable("id") String id,
                                  @RequestBody ShopOrder t,
                                  @RequestHeader String token) {
        ShopOrder orderUpdated = shopOrderService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,orderUpdated);
    }

    @Override
    public ResultsJson deleteById(@PathVariable("id") String id,
                                  @RequestHeader String token) {
        boolean result = shopOrderService.updateEntityStatusById(id, EntityStatus.DELETED);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id,
                                     @RequestHeader String token) {
        boolean result = shopOrderService.updateEntityStatusById(id, EntityStatus.FORBIDDEN);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson add(@RequestBody ShopOrder t,
                           @RequestHeader String token) {
        ShopOrder shopOrderAdded = shopOrderService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,shopOrderAdded);
    }

    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,shopOrderService.getCount());
    }

}
