package edu.hubu.web.controller;

import edu.hubu.web.commons.ResultsJson;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author moonlan
 * date 2020/10/8 12:00
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @NotNull
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, @NotNull HttpHeaders headers, HttpStatus status, @NotNull WebRequest request) {
        return new ResponseEntity<>(ResultsJson.toJson(status.value(), ex.getMessage(), 1, 0, null), HttpStatus.valueOf(200));
    }

    @RestController
    public static class GlobalExceptionController extends BasicErrorController {

        public GlobalExceptionController(ServerProperties serverProperties) {
            super(new DefaultErrorAttributes(), serverProperties.getError());
        }

        /**
         * 覆盖默认的Json响应
         */
        @ResponseStatus(HttpStatus.OK)
        @RequestMapping("/error")
        @Override
        public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
            HttpStatus status = getStatus(request);
            Map<String, Object> map = new HashMap<String, Object>();
            switch (status) {
                case OK:
                    return new ResponseEntity<>(map, HttpStatus.OK);
                case FORBIDDEN:
                    map.put("code", HttpStatus.FORBIDDEN.value());
                    map.put("msg", "认证失败");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                case INTERNAL_SERVER_ERROR:
                    map.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
                    map.put("msg", "服务器内部错误");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                case UNAUTHORIZED:
                    map.put("code", HttpStatus.UNAUTHORIZED.value());
                    map.put("msg", "未认证操作");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                case NOT_FOUND:
                    map.put("code", HttpStatus.NOT_FOUND.value());
                    map.put("msg", "未找到相关资源");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                case METHOD_NOT_ALLOWED:
                    map.put("code", HttpStatus.METHOD_NOT_ALLOWED.value());
                    map.put("msg", "请求方法错误");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                default:
                    map.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
                    map.put("msg", "未知错误");
                    map.put("total", 0);
                    map.put("current", 0);
                    map.put("data", null);
                    return new ResponseEntity<>(map, HttpStatus.OK);
            }

        }

        @ResponseStatus(HttpStatus.OK)
        @RequestMapping(produces = {"text/html"})
        @Override
        public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
            //请求的状态
            HttpStatus status = getStatus(request);
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json;charset=utf-8");
            switch (status) {
                case OK:
                    break;
                case FORBIDDEN:
                    ResultsJson.toJson(403, "权限不足", 1, 0, null, response);
                    break;
                case INTERNAL_SERVER_ERROR:
                    ResultsJson.toJson(500, "服务器内部错误", 1, 0, null, response);
                    break;
                case UNAUTHORIZED:
                    ResultsJson.toJson(401, "未认证操作", 1, 0, null, response);
                    break;
                case NOT_FOUND:
                    ResultsJson.toJson(404, "未找到相关信息", 1, 0, null, response);
                    break;
                case METHOD_NOT_ALLOWED:
                    ResultsJson.toJson(405, "请求方法错误", 1, 0, null, response);
                    break;
                default:
                    ResultsJson.toJson(512, "未知错误，请联系管理员", 1, 0, null, response);
                    break;
            }
            return null;
        }
    }
}
