package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.CommodityOption;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author waxijiang
 */
@RequestMapping("/commodityOption")
@RestController
public class CommodityOptionController extends CurdController<CommodityOption> {

    private final DefaultService<CommodityOption, String> commodityOptionService;

    public CommodityOptionController(DefaultService<CommodityOption, String> commodityOptionService) {
        this.commodityOptionService = commodityOptionService;
    }

    @Override
    protected CommodityOption register() {
        return new CommodityOption();
    }

    @PreAuthorize("hasAnyAuthority('findCommodityOption', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        CommodityOption entity = commodityOptionService.findEntityById(id,
                o -> ObjectUtils.Companion.reflectCheckFieldValue(o,
                        Objects.requireNonNull(commodityOptionService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(commodityOptionService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }


    @PreAuthorize("hasAnyAuthority('findCommodityOption', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage,
                                      @RequestParam(value = "size", defaultValue = "8") int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<CommodityOption> paged = commodityOptionService.getEntitiesPaged(currentPage, size, t, commodityOption -> ObjectUtils.Companion.reflectCheckFieldValue(commodityOption, Objects.requireNonNull(commodityOptionService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(commodityOption, Objects.requireNonNull(commodityOptionService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('findCommodityOption', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<CommodityOption> paged = commodityOptionService.findEntitiesPaged(currentPage, size, null);
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('updateCommodityOption', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody CommodityOption t, @RequestHeader String token) {
        CommodityOption user = commodityOptionService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, user);
    }

    @PreAuthorize("hasAnyAuthority('deleteCommodityOption', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commodityOptionService.logicDeleteEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenCommodityOption', 'admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commodityOptionService.forbidEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addCommodityOption', 'admin')")
    @Override
    public ResultsJson add(@RequestBody CommodityOption t, @RequestHeader String token) {
        CommodityOption role = commodityOptionService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countCommodityOption', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, commodityOptionService.getCount());
    }
}
