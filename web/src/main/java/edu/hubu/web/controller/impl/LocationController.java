package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Location;
import edu.hubu.web.service.DefaultService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
@RequestMapping("/location")
@RestController
public class LocationController extends CurdController<Location> {
    private final DefaultService<Location,String> locationService;

    public LocationController(DefaultService<Location, String> locationService) {
        this.locationService = locationService;
    }

    @Override
    protected Location register() {
        return new Location();
    }

    @Override
    public ResultsJson findOneById(@PathVariable("id") String id,@RequestHeader String token) {
        Location locationById = locationService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(locationService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(locationService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,locationById);

    }

    @Override
    public ResultsJson findByLikePage(@RequestParam(required = false)int currentPage,
                                      @RequestParam(required = false) int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<Location> locationPageData= locationService.getEntitiesPaged(currentPage, size, t, location -> ObjectUtils.Companion.reflectCheckFieldValue(location, Objects.requireNonNull(locationService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(location, Objects.requireNonNull(locationService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, locationPageData.getTotalSize(),currentPage,locationPageData);
    }

    @Override
    public ResultsJson findAllPage(@RequestParam(required = false) int currentPage,
                                   @RequestParam(required = false) int size,
                                   @RequestHeader String token) {
        PageData<Location> locationAll = locationService.getEntitiesPaged(currentPage, size, null, location -> ObjectUtils.Companion.reflectCheckFieldValue(location, Objects.requireNonNull(locationService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(location, Objects.requireNonNull(locationService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,locationAll.getTotalSize(),currentPage,locationAll);
    }


    @Override
    public ResultsJson updateById(@PathVariable("id") String id,
                                  @RequestBody Location t,
                                  @RequestHeader String token) {
        Location locationUpdated = locationService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,locationUpdated);
    }

    @Override
    public ResultsJson deleteById(@PathVariable("id") String id,
                                  @RequestHeader String token) {
        boolean result = locationService.updateEntityStatusById(id, EntityStatus.DELETED);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id,
                                     @RequestHeader String token) {
        boolean result = locationService.updateEntityStatusById(id, EntityStatus.FORBIDDEN);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson add(@RequestBody Location t,
                           @RequestHeader String token) {
        Location locationAdded = locationService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,locationAdded);
    }

    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,locationService.getCount());
    }
}
