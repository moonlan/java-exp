package edu.hubu.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Comment;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author waxijiang
 */
@RequestMapping("/comment")
@RestController
public class CommentController extends CurdController<Comment> {

    private final DefaultService<Comment, String> commentService;

    private final ObjectMapper mapper;

    public CommentController(DefaultService<Comment, String> commentService, ObjectMapper mapper) {
        this.commentService = commentService;
        this.mapper = mapper;
    }

    @Override
    protected Comment register() {
        return new Comment();
    }

    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        Comment entity = commentService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o,
                Objects.requireNonNull(commentService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(commentService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }


    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage,
                                      @RequestParam(value = "size", defaultValue = "8") int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<Comment> paged = commentService.getEntitiesPaged(currentPage, size, t, comment -> ObjectUtils.Companion.reflectCheckFieldValue(comment, Objects.requireNonNull(commentService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(comment, Objects.requireNonNull(commentService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<Comment> paged = commentService.findEntitiesPaged(currentPage, size, null);
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('updateComment', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody Comment t, @RequestHeader String token) {
        Comment user = commentService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, user);
    }

    @PreAuthorize("hasAnyAuthority('deleteComment', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commentService.logicDeleteEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenComment', 'admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commentService.forbidEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addComment', 'admin')")
    @Override
    public ResultsJson add(@RequestBody Comment t, @RequestHeader String token) {
        Comment role = commentService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countComment', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,commentService.getCount());
    }
}
