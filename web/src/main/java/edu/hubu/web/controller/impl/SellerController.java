package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Seller;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
@RequestMapping("/seller")
@RestController
public class SellerController extends CurdController<Seller> {

    private final DefaultService<Seller,String> sellerService;

    public SellerController(DefaultService<Seller, String> sellerService) {
        this.sellerService = sellerService;
    }

    @Override
    protected Seller register() {
        return new Seller();
    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson findOneById(@PathVariable("id") String id,@RequestHeader String token) {
        Seller sellerById = sellerService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(sellerService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(sellerService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,sellerById);

    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson findByLikePage(@RequestParam(required = false)int currentPage,
                                      @RequestParam(required = false) int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<Seller> entitiesPaged = sellerService.getEntitiesPaged(currentPage, size, t, seller -> ObjectUtils.Companion.reflectCheckFieldValue(seller, Objects.requireNonNull(sellerService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(seller, Objects.requireNonNull(sellerService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, entitiesPaged.getTotalSize(),currentPage,entitiesPaged);
    }
    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson findAllPage(@RequestParam(required = false) int currentPage,
                                   @RequestParam(required = false) int size,
                                   @RequestHeader String token) {
        PageData<Seller> sellerAll = sellerService.getEntitiesPaged(currentPage, size, null, role -> ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(sellerService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(sellerService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,sellerAll.getTotalSize(),currentPage,sellerAll);
    }


    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson updateById(@PathVariable("id") String id,
                                  @RequestBody Seller t,
                                  @RequestHeader String token) {

        Seller sellerUpdated = sellerService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,sellerUpdated);
    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson deleteById(@PathVariable("id") String id,
                                  @RequestHeader String token) {
        boolean result = sellerService.updateEntityStatusById(id, EntityStatus.DELETED);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson forbiddenById(@PathVariable("id") String id,
                                     @RequestHeader String token) {
        boolean result = sellerService.updateEntityStatusById(id, EntityStatus.FORBIDDEN);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson add(@RequestBody Seller t,
                           @RequestHeader String token) {
        Seller sellerAdded = sellerService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,sellerAdded);
    }

    @Override
    @PreAuthorize("hasAnyAuthority('findSeller', 'admin')")
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,sellerService.getCount());
    }


}
