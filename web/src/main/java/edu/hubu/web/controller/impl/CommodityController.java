package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Commodity;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author waxijiang
 */
@RequestMapping("/commodity")
@RestController
public class CommodityController extends CurdController<Commodity> {

    private final DefaultService<Commodity, String> commodityService;

    public CommodityController(DefaultService<Commodity, String> commodityService) {
        this.commodityService = commodityService;
    }

    @Override
    protected Commodity register() {
        return new Commodity();
    }

    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        Commodity entity = commodityService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o,
                Objects.requireNonNull(commodityService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(commodityService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }


    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage,
                                      @RequestParam(value = "size", defaultValue = "8") int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<Commodity> paged = commodityService.getEntitiesPaged(currentPage, size, t, commodity -> ObjectUtils.Companion.reflectCheckFieldValue(commodity, Objects.requireNonNull(commodityService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(commodity, Objects.requireNonNull(commodityService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('findComment', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<Commodity> paged = commodityService.findEntitiesPaged(currentPage, size, null);
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('updateComment', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody Commodity t, @RequestHeader String token) {
        Commodity user = commodityService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, user);
    }

    @PreAuthorize("hasAnyAuthority('deleteComment', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commodityService.logicDeleteEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenComment', 'admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = commodityService.forbidEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addComment', 'admin')")
    @Override
    public ResultsJson add(@RequestBody Commodity t, @RequestHeader String token) {
        Commodity role = commodityService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countComment', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, commodityService.getCount());
    }
}
