package edu.hubu.web.controller;

import edu.hubu.web.commons.ResultsJson;
import edu.hubu.web.commons.TableInfo;
import edu.hubu.web.model.BaseEntity;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * @author moonlan
 * date 2020/10/20 下午4:21
 */
@Transactional
public abstract class CurdController<EntityType extends BaseEntity> {

    @ApiOperation(value = "", hidden = true)
//    @RequestMapping("/register")
    @PreAuthorize("hasAnyAuthority('non')")
    protected abstract EntityType register();

    @GetMapping("/search/{id}")
    @ApiOperation("精确查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataType = "String", required = true)})
    public abstract ResultsJson findOneById(String id, String token);

    @PostMapping("/search_like")
    @ApiOperation("模糊分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "currentPage", value = "当前页，从1开始，默认为0", dataType = "int", required = false), @ApiImplicitParam(name = "size", value = "每页显示条数，默认8", dataType = "int", required = false), @ApiImplicitParam(name = "t", value = "需要查询的对象", required = true)})
    public abstract ResultsJson findByLikePage(int currentPage, int size, TableInfo t, String token);

    @GetMapping("/search")
    @ApiOperation("分页查询所有")
    @ApiImplicitParams({@ApiImplicitParam(name = "currentPage", value = "当前页，从1开始，默认为0", dataType = "int", required = false), @ApiImplicitParam(name = "size", value = "每页显示条数，默认8", dataType = "int", required = false),})
    public abstract ResultsJson findAllPage(int currentPage, int size, String token);

    @PutMapping("/{id}")
    @ApiOperation("根据id修改")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataType = "String", required = true), @ApiImplicitParam(name = "T", value = "需要修改的对象", dataType = "随着对象的不同而不同", required = true),})
    public abstract ResultsJson updateById(String id, EntityType t, String token);

    @DeleteMapping("/{id}")
    @ApiOperation("根据id删除")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataType = "String", required = true)})
    public abstract ResultsJson deleteById(String id, String token);

    @PatchMapping("/{id}")
    @ApiOperation("根据id封禁")
    public abstract ResultsJson forbiddenById(String id, String token);

    @PostMapping(value = "/", produces = "application/json")
    @ApiOperation("添加")
    @ApiImplicitParams({@ApiImplicitParam(name = "T", value = "需要修改的对象", dataType = "随着对象的不同而不同", required = true),})
    public abstract ResultsJson add(EntityType t, String token);

    @GetMapping("/count")
    @ApiOperation("该模型的所有数据的数量")
    public abstract ResultsJson count(String token);

}
