package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Role;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author moonlan
 * date 2020/11/26 12:21
 */
@SuppressWarnings(value = "unused")
@RequestMapping("/role")
@RestController
public class RoleController extends CurdController<Role> {
    private final DefaultService<Role, String> roleService;


    public RoleController(DefaultService<Role, String> roleService) {
        this.roleService = roleService;
    }


    @Override
    protected Role register() {
        return new Role();
    }

    @PreAuthorize("hasAnyAuthority('findRole', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        Role entity = roleService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(roleService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(roleService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('findRole', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestBody TableInfo t, @RequestHeader String token) {
        PageData<Role> paged = roleService.getEntitiesPaged(currentPage, size, t, role -> ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(roleService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(roleService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());
    }

    @PreAuthorize("hasAnyAuthority('findRole', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<Role> paged = roleService.getEntitiesPaged(currentPage, size, null, role -> ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(roleService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(role, Objects.requireNonNull(roleService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());
    }

    @PreAuthorize("hasAnyAuthority('updateRole', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody Role t, @RequestHeader String token) {
        Role role = roleService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('deleteRole', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = roleService.updateEntityStatusById(id, EntityStatus.DELETED);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenRole', 'admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = roleService.updateEntityStatusById(id, EntityStatus.FORBIDDEN);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addRole', 'admin')")
    @Override
    public ResultsJson add(@RequestBody Role t, @RequestHeader String token) {
        Role role = roleService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countRole', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        int count = roleService.getCount();
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, count);
    }
}
