package edu.hubu.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.Buyer;
import edu.hubu.web.model.User;
import edu.hubu.web.service.DefaultService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author waxijiang
 */
@RequestMapping("/buyer")
@RestController
public class BuyerController extends CurdController<Buyer> {

    private final DefaultService<Buyer, String> buyerService;

    private final ObjectMapper mapper;

    private final BCryptPasswordEncoder encoder;

    public BuyerController(DefaultService<Buyer, String> buyerService, ObjectMapper mapper, BCryptPasswordEncoder encoder) {
        this.buyerService = buyerService;
        this.mapper = mapper;
        this.encoder = encoder;
    }

    @Override
    protected Buyer register() {
        return new Buyer();
    }

    @PreAuthorize("hasAnyAuthority('findBuyer', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        User entity = buyerService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(buyerService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(buyerService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }


    @PreAuthorize("hasAnyAuthority('findBuyer', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, TableInfo t, @RequestHeader String token) {
        PageData<Buyer> paged = buyerService.getEntitiesPaged(currentPage, size, t, buyer -> ObjectUtils.Companion.reflectCheckFieldValue(buyer, Objects.requireNonNull(buyerService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(buyer, Objects.requireNonNull(buyerService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('findBuyer', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<Buyer> paged = buyerService.getEntitiesPaged(currentPage, size, null,buyer -> ObjectUtils.Companion.reflectCheckFieldValue(buyer, Objects.requireNonNull(buyerService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(buyer, Objects.requireNonNull(buyerService.getForbiddenFieldName()), false));

        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('updateBuyer', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody Buyer t, @RequestHeader String token) {
        User user = buyerService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, user);
    }

    @PreAuthorize("hasAnyAuthority('deleteBuyer', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = buyerService.logicDeleteEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenBuyer', 'admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = buyerService.forbidEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addBuyer', 'admin')")
    @Override
    public ResultsJson add(@RequestBody Buyer t, @RequestHeader String token) {
        String userPassword = t.getUserPassword();
        t.setUserPassword(encoder.encode(userPassword));
        User role = buyerService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countBuyer', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, buyerService.getCount());
    }
}
