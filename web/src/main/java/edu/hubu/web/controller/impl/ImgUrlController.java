package edu.hubu.web.controller.impl;

import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.ImgUrl;
import edu.hubu.web.service.DefaultService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
@RequestMapping("/imgUrl")
@RestController
public class ImgUrlController extends CurdController<ImgUrl> {
    private final DefaultService<ImgUrl,String> imgUrlService;

    public ImgUrlController(DefaultService<ImgUrl, String> imgUrlService) {
        this.imgUrlService = imgUrlService;
    }

    @Override
    protected ImgUrl register() {
        return new ImgUrl();
    }

    @Override
    public ResultsJson findOneById(@PathVariable("id") String id,@RequestHeader String token) {
        ImgUrl imgUrlById = imgUrlService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(imgUrlService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(imgUrlService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,imgUrlById);

    }

    @Override
    public ResultsJson findByLikePage(@RequestParam(required = false)int currentPage,
                                      @RequestParam(required = false) int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<ImgUrl> imgUrlPageData= imgUrlService.getEntitiesPaged(currentPage, size, t, imgUrl -> ObjectUtils.Companion.reflectCheckFieldValue(imgUrl, Objects.requireNonNull(imgUrlService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(imgUrl, Objects.requireNonNull(imgUrlService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, imgUrlPageData.getTotalSize(),currentPage,imgUrlPageData);
    }

    @Override
    public ResultsJson findAllPage(@RequestParam(required = false) int currentPage,
                                   @RequestParam(required = false) int size,
                                   @RequestHeader String token) {
        PageData<ImgUrl> imgUrlAll = imgUrlService.getEntitiesPaged(currentPage, size, null, imgUrl -> ObjectUtils.Companion.reflectCheckFieldValue(imgUrl, Objects.requireNonNull(imgUrlService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(imgUrl, Objects.requireNonNull(imgUrlService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS,imgUrlAll.getTotalSize(),currentPage,imgUrlAll);
    }


    @Override
    public ResultsJson updateById(@PathVariable("id") String id,
                                  @RequestBody ImgUrl t,
                                  @RequestHeader String token) {
        ImgUrl imgUrlUpdated = imgUrlService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,imgUrlUpdated);
    }

    @Override
    public ResultsJson deleteById(@PathVariable("id") String id,
                                  @RequestHeader String token) {
        boolean result = imgUrlService.updateEntityStatusById(id, EntityStatus.DELETED);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id,
                                     @RequestHeader String token) {
        boolean result = imgUrlService.updateEntityStatusById(id, EntityStatus.FORBIDDEN);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,result);
    }

    @Override
    public ResultsJson add(@RequestBody ImgUrl t,
                           @RequestHeader String token) {
        ImgUrl imgUrlAdded = imgUrlService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,imgUrlAdded);
    }

    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS,1,0,imgUrlService.getCount());
    }
}
