package edu.hubu.web.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.*;
import edu.hubu.web.controller.CurdController;
import edu.hubu.web.model.User;
import edu.hubu.web.service.DefaultService;
import edu.hubu.web.service.impl.UserServiceImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author waxijiang
 */
@RequestMapping("/user")
@RestController
public class UserController extends CurdController<User> {

    private final UserServiceImpl userService;

    private final ObjectMapper mapper;

    private final BCryptPasswordEncoder encoder;

    public UserController(DefaultService<User,String> userService, ObjectMapper mapper, BCryptPasswordEncoder encoder) {
        this.userService = (UserServiceImpl) userService;
        this.mapper = mapper;
        this.encoder = encoder;
    }

    @PostMapping("/sendEmail")
    public ResultsJson sendEmail(@RequestParam String email){
        return userService.sendEmail(email);

    }
    @PostMapping("/register")
    public ResultsJson registerUser(@RequestBody User user,@RequestParam String verifyCode){
        return userService.register(user,verifyCode);
    }

    @Override
    protected User register() {
        return new User();
    }

    @PreAuthorize("hasAnyAuthority('findUser', 'admin')")
    @Override
    public ResultsJson findOneById(@PathVariable("id") String id, @RequestHeader String token) {
        User entity = userService.findEntityById(id, o -> ObjectUtils.Companion.reflectCheckFieldValue(o,
                Objects.requireNonNull(userService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(userService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }


    @PreAuthorize("hasAnyAuthority('findUser', 'admin')")
    @Override
    public ResultsJson findByLikePage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage,
                                      @RequestParam(value = "size", defaultValue = "8") int size,@RequestBody TableInfo t,
                                      @RequestHeader String token) {
        PageData<User> paged = userService.getEntitiesPaged(currentPage, size, t, user -> ObjectUtils.Companion.reflectCheckFieldValue(user, Objects.requireNonNull(userService.getDeleteFieldName()), false)
                && ObjectUtils.Companion.reflectCheckFieldValue(user, Objects.requireNonNull(userService.getForbiddenFieldName()), false));
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('findUser', 'admin')")
    @Override
    public ResultsJson findAllPage(@RequestParam(value = "currentPage", defaultValue = "0") int currentPage, @RequestParam(value = "size", defaultValue = "8") int size, @RequestHeader String token) {
        PageData<User> paged = userService.findEntitiesPaged(currentPage, size, null);
        return ResultsJson.toJson(StatusCode.SUCCESS, paged.getTotalSize(), currentPage, paged.getData());

    }

    @PreAuthorize("hasAnyAuthority('updateUser', 'admin')")
    @Override
    public ResultsJson updateById(@PathVariable("id") String id, @RequestBody User t, @RequestHeader String token) {
        User user = userService.updateEntityById(id, t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, user);
    }

    @PreAuthorize("hasAnyAuthority('deleteUser', 'admin')")
    @Override
    public ResultsJson deleteById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = userService.logicDeleteEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('forbiddenUser','admin')")
    @Override
    public ResultsJson forbiddenById(@PathVariable("id") String id, @RequestHeader String token) {
        boolean entity = userService.forbidEntityById(id);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, entity);
    }

    @PreAuthorize("hasAnyAuthority('addUser', 'admin')")
    @Override
    public ResultsJson add(@RequestBody User t, @RequestHeader String token) {
        String userPassword = t.getUserPassword();
        t.setUserPassword(encoder.encode(userPassword));
        User role = userService.addEntity(t);
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, role);
    }

    @PreAuthorize("hasAnyAuthority('countUser', 'admin')")
    @Override
    public ResultsJson count(@RequestHeader String token) {
        return ResultsJson.toJson(StatusCode.SUCCESS, 1, 0, userService.getCount());
    }
}
