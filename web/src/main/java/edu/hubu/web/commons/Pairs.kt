@file:JvmName("Pair")

package edu.hubu.web.commons

import kotlin.Pair

class Pairs<A, B> {
    var first: A? = null
    var second: B? = null

    constructor() {}

    constructor(first: A?, second: B?) {
        this.first = first
        this.second = second
    }

    override fun toString(): String = "($first, $second)"
}