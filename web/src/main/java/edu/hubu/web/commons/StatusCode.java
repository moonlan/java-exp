package edu.hubu.web.commons;


import edu.hubu.web.exception.*;

/**
 * @author moonlan
 * @date 2020/10/8 11:57
 */
public enum StatusCode {
    //成功
    SUCCESS(200, "请求成功"),
    //客户端
    SYNTAX_ERROR(400, "客户端请求语法错误" , SyntaxErrorException.class),
    UN_AUTHORIZED(401, "用户未认证", UnAuthorizedException.class),
    NO_PERMISSION(403, "权限不足", PermissionDeniedException.class),
    NOT_FOUND(404, "未找到相关资源", InformationNotFoundException.class),
    METHOD_ERROR(405, "请求方法错误", MethodNotValidException.class),
    UNKNOWN_USERNAME(410, "用户名未找到", UnknowUsernameException.class),
    WRONG_USERNAME_OR_PASSWORD(411, "用户名或密码错误", WrongUsernameOrPasswordException.class),
    //服务端错误
    INTERNAL_SERVER_ERROR(500, "服务器内部错误", InternalServerException.class);


    private final int code;
    private final String msg;
    private final Class<? extends Exception> exceptionClass;

    StatusCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.exceptionClass = Exception.class;
    }

    StatusCode(int code, String msg, Class<? extends Exception> exceptionClass) {
        this.code = code;
        this.msg = msg;
        this.exceptionClass = exceptionClass;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Class<? extends Exception> getExceptionClass() {
        return exceptionClass;
    }
}
