package edu.hubu.web.commons

/**
 * @author moonlan
 * date 2020/11/30 20:48
 */
enum class EntityStatus {
    NOT_FORBIDDEN, NOT_DELETED, FORBIDDEN, DELETED;
}