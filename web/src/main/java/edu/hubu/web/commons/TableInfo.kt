@file:JvmName("TableInfo")

package edu.hubu.web.commons

import edu.hubu.web.service.DefaultService
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

class TableInfo {
    var pkName: Pairs<String, Any> = Pairs()
    var normalColumns: ArrayList<Pairs<String, Any>> = ArrayList()
    var fkColumns: ArrayList<Triples<String, String, Any>> = ArrayList()


    constructor() {

    }

    constructor(
        pkName: Pairs<String, Any>,
        normalColumns: ArrayList<Pairs<String, Any>>,
        fkColumns: ArrayList<Triples<String, String, Any>>
    ) {
        this.pkName = pkName
        this.normalColumns = normalColumns
        this.fkColumns = fkColumns
    }
}