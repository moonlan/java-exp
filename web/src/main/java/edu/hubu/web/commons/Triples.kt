@file:JvmName("Triple")

package edu.hubu.web.commons

class Triples<A, B, C> {
    var first: A? = null
    var second: B? = null
    var third: C? = null

    constructor() {}

    constructor(first: A?, second: B?, third: C?) {
        this.first = first
        this.second = second
        this.third = third
    }

    override fun toString(): String = "($first, $second, $third)"
    
    
}