package edu.hubu.web.commons

import lombok.Builder
import lombok.Data

/**
 * @author moonlan
 * date 2020/11/26 13:14
 */
data class PageData<T>(var totalPage: Int?, var totalSize: Int?, var currentPage: Int?, var data: List<T>?)