package edu.hubu.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.ObjectUtils;
import edu.hubu.web.dao.impl.IUserDao;
import edu.hubu.web.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author moonlan
 * date 2020/11/28 19:11
 */
@SuppressWarnings(value = "unused")
@SpringBootTest
public class ObjectUtilsTest {
    @Autowired
    IUserDao userDao;

    @Autowired
    ObjectMapper mapper;

    @Test
    @Transactional
    void testFilter() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        User filter = ObjectUtils.Companion.filter(one);
        System.out.println(mapper.writeValueAsString(filter));
    }

    @Test
    @Transactional
    void testToMap() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        var filter = ObjectUtils.Companion.toMap(one);
        System.out.println(filter);
    }

    @Test
    @Transactional
    void testAssign() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        User user = new User();
        user.setEntityIsDeleted(true);
        ObjectUtils.Companion.assign(user, one);
        System.out.println(mapper.writeValueAsString(one));
    }

    @Test
    @Transactional
    void testReflectCheckFieldValue() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        boolean isDeleted = ObjectUtils.Companion.reflectCheckFieldValue(one, "isEntityIsDeleted", false);
        System.out.println(mapper.writeValueAsString(isDeleted));
    }

    @Test
    @Transactional
    void testReflectGetFieldValue() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        var isDeleted = ObjectUtils.Companion.reflectGetFieldValue(one, "userCreateTime");
        System.out.println(mapper.writeValueAsString(isDeleted));
    }

    @Test
    @Transactional
    void testReflectSetFieldValue() throws JsonProcessingException {
        User one = userDao.findById("USER-2020-11111111").get();
        ObjectUtils.Companion.reflectSetFieldValue(one, "isEntityIsDeleted", true);
        System.out.println(mapper.writeValueAsString(one));
    }
}
