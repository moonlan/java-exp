package edu.hubu.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.ObjectUtils;
import edu.hubu.web.model.*;
import edu.hubu.web.service.DefaultService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@SpringBootTest
@TestMethodOrder(MethodOrderer.DisplayName.class)
class WebApplicationTests {
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private ObjectMapper mapper;

    private final String imgUrl = "http://localhost:40000/img/upload/";
    @Autowired
    DefaultService<Role, String> roleService;
    @Autowired
    DefaultService<Location, String> locationService;
    @Autowired
    DefaultService<User, String> userService;
    @Autowired
    DefaultService<Seller, String> sellerService;
    @Autowired
    DefaultService<Buyer, String> buyerService;
    @Autowired
    DefaultService<Commodity, String> commodityService;
    @Autowired
    DefaultService<ImgUrl, String> imgUrlService;
    @Autowired
    DefaultService<Comment, String> commentService;
    @Autowired
    DefaultService<CommodityOption, String> commodityOptionService;
    @Autowired
    DefaultService<Category, String> categoryService;
    @Autowired
    DefaultService<ShopOrder, String> shopOrderService;

    @Test
    void createTable() {
    }

    @Test
    void initTable() {
        Role role1 = new Role();
        role1.setRoleName("ADMIN");
        role1.setRoleId(role1.getUUID("role", "11111111"));
        roleService.addEntity(role1);

        Role role2 = new Role();
        role2.setRoleName("BUYER");
        role2.setRoleId(role2.getUUID("role", "22222222"));
        roleService.addEntity(role2);

        Role role3 = new Role();
        role3.setRoleName("SELLER");
        role3.setRoleId(role3.getUUID("role", "33333333"));
        roleService.addEntity(role3);


        Location location1 = new Location();
        location1.setLocationContent("武汉市洪山区");
        location1.setLocationId(location1.getUUID("location", "11111111"));
        locationService.addEntity(location1);


        User user1 = new User();
        user1.setUserId(user1.getUUID("user", "11111111"));
        user1.setUserPassword(encoder.encode("HelloWorld"));
        user1.setUserName("moon");
        user1.setUserTelephoneNumber("13476011234");
        user1.setUserEmail("2108021234@qq.com");
        //        user1.setRole(role1);
        user1.getRoles().add(role1);
        userService.addEntity(user1);


        Seller seller1 = new Seller();
        seller1.setUserId(seller1.getUUID("seller", "22222222"));
        seller1.setUserPassword(encoder.encode("HelloWorld"));
        seller1.setUserName("fyw");
        seller1.setUserTelephoneNumber("18976011234");
        seller1.setUserEmail("2208021234@qq.com");
        seller1.getRoles().add(role3);
        //        seller1.setRole(role3);
        seller1.getLocation().add(location1);
        sellerService.addEntity(seller1);

        Seller seller2 = new Seller();
        seller2.setUserId(seller2.getUUID("seller", "33333333"));
        seller2.setUserPassword(encoder.encode("HelloWorld"));
        seller2.setUserName("csy");
        seller2.setUserTelephoneNumber("15976011234");
        seller2.setUserEmail("2308021234@qq.com");
        seller2.getRoles().add(role3);
        //        seller2.setRole(role3);
        seller2.getLocation().add(location1);
        sellerService.addEntity(seller2);

        Seller seller3 = new Seller();
        seller3.setUserId(seller3.getUUID("seller", "44444444"));
        seller3.setUserPassword(encoder.encode("HelloWorld"));
        seller3.setUserName("cxl");
        seller3.setUserTelephoneNumber("15986011234");
        seller3.setUserEmail("2408021234@qq.com");
        seller3.getRoles().add(role3);
        //        seller3.setRole(role3);
        seller3.getLocation().add(location1);
        sellerService.addEntity(seller3);

        Seller seller4 = new Seller();
        seller4.setUserId(seller4.getUUID("seller", "55555555"));
        seller4.setUserPassword(encoder.encode("HelloWorld"));
        seller4.setUserName("hjy");
        seller4.setUserTelephoneNumber("13476011255");
        seller4.setUserEmail("2508021234@qq.com");
        seller4.getRoles().add(role3);
        //        seller4.setRole(role3);
        seller4.getLocation().add(location1);
        sellerService.addEntity(seller4);


        Buyer buyer1 = new Buyer();
        buyer1.setUserId(buyer1.getUUID("buyer", "66666666"));
        buyer1.setUserPassword(encoder.encode("HelloWorld"));
        buyer1.setUserName("rsl");
        buyer1.setUserTelephoneNumber("13479011234");
        buyer1.setUserEmail("2908021234@qq.com");
        buyer1.getRoles().add(role2);
        buyer1.getLocation().add(location1);
        buyerService.addEntity(buyer1);

        Buyer buyer2 = new Buyer();
        buyer2.setUserId(buyer2.getUUID("buyer", "77777777"));
        buyer2.setUserPassword(encoder.encode("HelloWorld"));
        buyer2.setUserName("hxx");
        buyer2.setUserTelephoneNumber("13480011234");
        buyer2.setUserEmail("2118021234@qq.com");
        buyer2.getRoles().add(role2);
        buyer2.getLocation().add(location1);
        buyerService.addEntity(buyer2);

        Buyer buyer3 = new Buyer();
        buyer3.setUserId(buyer3.getUUID("buyer", "88888888"));
        buyer3.setUserPassword(encoder.encode("HelloWorld"));
        buyer3.setUserName("wl");
        buyer3.setUserTelephoneNumber("13476899234");
        buyer3.setUserEmail("2899021234@qq.com");
        buyer3.getRoles().add(role2);
        buyer3.getLocation().add(location1);
        buyerService.addEntity(buyer3);

        Buyer buyer4 = new Buyer();
        buyer4.setUserId(buyer4.getUUID("buyer", "99999999"));
        buyer4.setUserPassword(encoder.encode("HelloWorld"));
        buyer4.setUserName("lrn");
        buyer4.setUserTelephoneNumber("13476758234");
        buyer4.setUserEmail("2785421234@qq.com");
        buyer4.getRoles().add(role2);
        buyer4.getLocation().add(location1);
        buyerService.addEntity(buyer4);


        Category category1 = new Category();
        category1.setCategoryId(category1.getUUID("category", "11111111"));
        category1.setCategoryName("衣服");
        categoryService.addEntity(category1);

        Category category2 = new Category();
        category2.setCategoryId(category2.getUUID("category", "22222222"));
        category2.setCategoryName("电子产品");
        categoryService.addEntity(category2);

        Category category3 = new Category();
        category3.setCategoryId(category3.getUUID("category", "33333333"));
        category3.setCategoryName("日用品");
        categoryService.addEntity(category3);

        Commodity commodity1 = new Commodity();
        commodity1.setCommodityName("大衣");
        commodity1.setCommodityDescription("好大衣");
        commodity1.setCommodityDiscount(1.0);
        commodity1.setCommodityScore(10.0);
        commodity1.setSeller(seller1);
        commodity1.setCategory(category1);
        commodityService.addEntity(commodity1);

        Commodity commodity2 = new Commodity();
        commodity2.setCommodityName("鞋子");
        commodity2.setCommodityDescription("好鞋子");
        commodity2.setCommodityDiscount(1.0);
        commodity2.setCommodityScore(10.0);
        commodity2.setSeller(seller2);
        commodity2.setCategory(category1);
        commodityService.addEntity(commodity2);

        Commodity commodity3 = new Commodity();
        commodity3.setCommodityName("裤子");
        commodity3.setCommodityDescription("好裤子");
        commodity3.setCommodityDiscount(1.0);
        commodity3.setCommodityScore(10.0);
        commodity3.setSeller(seller3);
        commodity3.setCategory(category1);
        commodityService.addEntity(commodity3);

        Commodity commodity4 = new Commodity();
        commodity4.setCommodityName("帽子");
        commodity4.setCommodityDescription("好帽子");
        commodity4.setCommodityDiscount(1.0);
        commodity4.setCommodityScore(10.0);
        commodity4.setSeller(seller4);
        commodity4.setCategory(category1);
        commodityService.addEntity(commodity4);

        Commodity commodity5 = new Commodity();
        commodity5.setCommodityName("手机");
        commodity5.setCommodityDescription("好手机");
        commodity5.setCommodityDiscount(1.0);
        commodity5.setCommodityScore(10.0);
        commodity5.setSeller(seller1);
        commodity5.setCategory(category2);
        commodityService.addEntity(commodity5);

        Commodity commodity6 = new Commodity();
        commodity6.setCommodityName("电脑");
        commodity6.setCommodityDescription("好电脑");
        commodity6.setCommodityDiscount(1.0);
        commodity6.setCommodityScore(10.0);
        commodity6.setSeller(seller2);
        commodity6.setCategory(category2);
        commodityService.addEntity(commodity6);

        Commodity commodity7 = new Commodity();
        commodity7.setCommodityName("鼠标");
        commodity7.setCommodityDescription("好鼠标");
        commodity7.setCommodityDiscount(1.0);
        commodity7.setCommodityScore(10.0);
        commodity7.setSeller(seller3);
        commodity7.setCategory(category2);
        commodityService.addEntity(commodity7);

        Commodity commodity8 = new Commodity();
        commodity8.setCommodityName("键盘");
        commodity8.setCommodityDescription("好键盘");
        commodity8.setCommodityDiscount(1.0);
        commodity8.setCommodityScore(10.0);
        commodity8.setSeller(seller4);
        commodity8.setCategory(category2);
        commodityService.addEntity(commodity8);

        Commodity commodity9 = new Commodity();
        commodity9.setCommodityName("电扇");
        commodity9.setCommodityDescription("好电扇");
        commodity9.setCommodityDiscount(1.0);
        commodity9.setCommodityScore(10.0);
        commodity9.setSeller(seller1);
        commodity9.setCategory(category2);
        commodityService.addEntity(commodity9);


        ImgUrl imgUrl1 = new ImgUrl();
        imgUrl1.setImgUrl(imgUrl + "1.jpg");
        imgUrl1.setCommodity(commodity1);
        imgUrlService.addEntity(imgUrl1);

        ImgUrl imgUrl2 = new ImgUrl();
        imgUrl2.setImgUrl(imgUrl + "2.jpg");
        imgUrl2.setCommodity(commodity2);
        imgUrlService.addEntity(imgUrl2);

        ImgUrl imgUrl3 = new ImgUrl();
        imgUrl3.setImgUrl(imgUrl + "3.jpg");
        imgUrl3.setCommodity(commodity3);
        imgUrlService.addEntity(imgUrl3);

        ImgUrl imgUrl4 = new ImgUrl();
        imgUrl4.setImgUrl(imgUrl + "4.jpg");
        imgUrl4.setCommodity(commodity4);
        imgUrlService.addEntity(imgUrl4);

        ImgUrl imgUrl5 = new ImgUrl();
        imgUrl5.setImgUrl(imgUrl + "5.jpg");
        imgUrl5.setCommodity(commodity5);
        imgUrlService.addEntity(imgUrl5);

        ImgUrl imgUrl6 = new ImgUrl();
        imgUrl6.setImgUrl(imgUrl + "6.jpg");
        imgUrl6.setCommodity(commodity6);
        imgUrlService.addEntity(imgUrl6);

        ImgUrl imgUrl7 = new ImgUrl();
        imgUrl7.setImgUrl(imgUrl + "7.jpg");
        imgUrl7.setCommodity(commodity7);
        imgUrlService.addEntity(imgUrl7);

        ImgUrl imgUrl8 = new ImgUrl();
        imgUrl8.setImgUrl(imgUrl + "8.jpg");
        imgUrl8.setCommodity(commodity8);
        imgUrlService.addEntity(imgUrl8);

        ImgUrl imgUrl9 = new ImgUrl();
        imgUrl9.setImgUrl(imgUrl + "9.jpg");
        imgUrl9.setCommodity(commodity9);
        imgUrlService.addEntity(imgUrl9);


        Comment comment1 = new Comment();
        comment1.setCommentScoreToCommodity(5.0);
        comment1.setCommentAgreeNumber(100);
        comment1.setCommentContent("HelloWorld");
        comment1.setCommentIsAnonymous(false);
        comment1.setCommodity(commodity1);
        comment1.setUser(user1);
        commentService.addEntity(comment1);

        Comment comment2 = new Comment();
        comment2.setCommentScoreToCommodity(4.0);
        comment2.setCommentAgreeNumber(100);
        comment2.setCommentContent("HelloWorld");
        comment2.setCommentIsAnonymous(false);
        comment2.setCommodity(commodity2);
        comment2.setUser(seller1);
        commentService.addEntity(comment2);

        Comment comment3 = new Comment();
        comment3.setCommentScoreToCommodity(3.0);
        comment3.setCommentAgreeNumber(100);
        comment3.setCommentContent("HelloWorld");
        comment3.setCommentIsAnonymous(false);
        comment3.setCommodity(commodity3);
        comment3.setUser(seller2);
        commentService.addEntity(comment3);

        Comment comment4 = new Comment();
        comment4.setCommentScoreToCommodity(2.0);
        comment4.setCommentAgreeNumber(100);
        comment4.setCommentContent("HelloWorld");
        comment4.setCommentIsAnonymous(false);
        comment4.setCommodity(commodity4);
        comment4.setUser(seller3);
        commentService.addEntity(comment4);

        Comment comment5 = new Comment();
        comment5.setCommentScoreToCommodity(1.0);
        comment5.setCommentAgreeNumber(100);
        comment5.setCommentContent("HelloWorld");
        comment5.setCommentIsAnonymous(false);
        comment5.setCommodity(commodity5);
        comment5.setUser(seller4);
        commentService.addEntity(comment5);

        Comment comment6 = new Comment();
        comment6.setCommentScoreToCommodity(6.0);
        comment6.setCommentAgreeNumber(100);
        comment6.setCommentContent("HelloWorld");
        comment6.setCommentIsAnonymous(false);
        comment6.setCommodity(commodity6);
        comment6.setUser(buyer1);
        commentService.addEntity(comment6);

        Comment comment7 = new Comment();
        comment7.setCommentScoreToCommodity(7.0);
        comment7.setCommentAgreeNumber(100);
        comment7.setCommentContent("HelloWorld");
        comment7.setCommentIsAnonymous(false);
        comment7.setCommodity(commodity7);
        comment7.setUser(buyer2);
        commentService.addEntity(comment7);

        Comment comment8 = new Comment();
        comment8.setCommentScoreToCommodity(8.0);
        comment8.setCommentAgreeNumber(100);
        comment8.setCommentContent("HelloWorld");
        comment8.setCommentIsAnonymous(false);
        comment8.setCommodity(commodity8);
        comment8.setUser(buyer3);
        commentService.addEntity(comment8);

        Comment comment9 = new Comment();
        comment9.setCommentScoreToCommodity(9.0);
        comment9.setCommentAgreeNumber(100);
        comment9.setCommentContent("HelloWorld");
        comment9.setCommentIsAnonymous(false);
        comment9.setCommodity(commodity9);
        comment9.setUser(buyer4);
        commentService.addEntity(comment9);


        CommodityOption option1 = new CommodityOption();
        option1.setCommodity(commodity1);
        option1.setCommodityOptionName("体型");
        option1.setCommodityOptionValue("大");
        commodityOptionService.addEntity(option1);

        CommodityOption option2 = new CommodityOption();
        option2.setCommodity(commodity1);
        option2.setCommodityOptionName("体型");
        option2.setCommodityOptionValue("巨大");
        commodityOptionService.addEntity(option2);

        CommodityOption option3 = new CommodityOption();
        option3.setCommodity(commodity2);
        option3.setCommodityOptionName("体型");
        option3.setCommodityOptionValue("中型");
        commodityOptionService.addEntity(option3);

        CommodityOption option4 = new CommodityOption();
        option4.setCommodity(commodity2);
        option4.setCommodityOptionName("体型");
        option4.setCommodityOptionValue("中小型");
        commodityOptionService.addEntity(option4);

        CommodityOption option5 = new CommodityOption();
        option5.setCommodity(commodity3);
        option5.setCommodityOptionName("体型");
        option5.setCommodityOptionValue("小");
        commodityOptionService.addEntity(option5);

        CommodityOption option6 = new CommodityOption();
        option6.setCommodity(commodity3);
        option6.setCommodityOptionName("体型");
        option6.setCommodityOptionValue("非常小");
        commodityOptionService.addEntity(option6);


        for (int i = 0; i < 5; i++) {
            ShopOrder shopOrder1 = new ShopOrder();
            shopOrder1.setBuyer(buyer1);
            shopOrder1.setCommodity(commodity1);
            shopOrder1.setLocation(location1);
            shopOrder1.setOrderDiscount(8.8);
            shopOrder1.setOrderMoney((double) (100 + i));
            shopOrder1.setOrderNumber((long) i);
            shopOrder1.setOrderTotalMoney(i * (double) (100 + i) * 0.88);
            shopOrderService.addEntity(shopOrder1);
        }
    }

    @Test
    @Transactional
    void testSelectAll() {
        roleService.findEntitiesPaged(0, 8, null).getData().stream().map(role -> {
            try {
                return mapper.writeValueAsString(role);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        locationService.findEntitiesPaged(0, 8, null).getData().stream().map(location -> {
            try {
                return mapper.writeValueAsString(location);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        userService.findEntitiesPaged(0, 8, null).getData().stream().map(baseUser -> {
            try {
                return mapper.writeValueAsString(baseUser);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        sellerService.findEntitiesPaged(0, 8, null).getData().stream().map(seller -> {
            try {
                return mapper.writeValueAsString(seller);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        buyerService.findEntitiesPaged(0, 8, null).getData().stream().map(buyer -> {
            try {
                return mapper.writeValueAsString(buyer);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        commodityService.findEntitiesPaged(0, 8, null).getData().stream().map(commodity -> {
            try {
                return mapper.writeValueAsString(commodity);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        imgUrlService.findEntitiesPaged(0, 8, null).getData().stream().map(imgUrl1 -> {
            try {
                return mapper.writeValueAsString(imgUrl1);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        commentService.findEntitiesPaged(0, 8, null).getData().stream().map(comment -> {
            try {
                return mapper.writeValueAsString(comment);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
        System.out.println("-----------------------------------");
        commodityOptionService.findEntitiesPaged(0, 8, null).getData().stream().map(commodityOption -> {
            try {
                return mapper.writeValueAsString(commodityOption);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
    }

    @Test
    @Transactional
    void testDelete() {
        boolean forbidEntityById = userService.deleteEntityById("USER-2020-11111111");
        System.out.println(forbidEntityById);
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testLogicDelete() {
        boolean forbidEntityById = buyerService.logicDeleteEntityById("BUYER-2020-99999999");
        System.out.println(forbidEntityById);
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testForbidden() {
        boolean forbidEntityById = userService.forbidEntityById("USER-2020-11111111");
        System.out.println(forbidEntityById);
    }

    @Test
    @Transactional
    void testSelectOne() throws JsonProcessingException {
        User entityById = sellerService.findEntityById("SELLER-2020-55555555", o -> ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(roleService.getDeleteFieldName()), false) && ObjectUtils.Companion.reflectCheckFieldValue(o, Objects.requireNonNull(roleService.getForbiddenFieldName()), false));
        System.out.println(mapper.writeValueAsString(entityById));
    }

    @Test
    @Transactional
    void testSelectPagedOne() {
        User user = new User();
        user.setUserId("USER-2020-11111111");
        List<User> userId = ObjectUtils.Companion.toList(userService.findEntitiesPaged(0, 8, user).getData(), User.class);
        userId.stream().map(baseUser -> {
            try {
                return mapper.writeValueAsString(baseUser);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
    }

}
