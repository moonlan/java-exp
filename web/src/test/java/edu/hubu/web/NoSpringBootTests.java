package edu.hubu.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.Pairs;
import edu.hubu.web.commons.TableInfo;
import edu.hubu.web.commons.Triples;
import edu.hubu.web.model.*;
import kotlin.Pair;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author moonlan
 * date 2020/11/26 8:09
 */
@SuppressWarnings(value = "unused")
public class NoSpringBootTests {
    @Test
    void test() {
        User user = new User();
        for (Field field : user.getClass().getDeclaredFields()) {
            System.out.println(field.getName());
        }
    }

    @Test
    void test2() throws NoSuchFieldException {
        User user = new User();
        System.out.println(user.getClass().getDeclaredField("isEntityIsDeleted"));
    }

    @Test
    void test3() {
//        Predicate<BaseUser> predicate = o -> ObjectUtils.is(o, "isEntityIsDeleted") && ObjectUtils.is(o, "isEntityIsForbidden");
//        BaseUser user = new BaseUser();
//        user.setEntityIsDeleted(true);
//        //        user.setEntityIsForbidden(true);
//        System.out.println(predicate.test(user));
    }

    @Test
    void test4() throws NoSuchFieldException, IllegalAccessException {
        Seller seller = new Seller();
        Field field = seller.getClass().getDeclaredField("commodities");
        field.setAccessible(true);
        Object o = field.get(seller);
        System.out.println(o);
    }

    @Test
    void test5() throws NoSuchFieldException, IllegalAccessException, IOException {
        Map<Class<? extends BaseEntity>, BaseEntity> list = Map.of(User.class, new User(), Buyer.class, new Buyer(), Comment.class, new Comment(), Commodity.class, new Commodity(), CommodityOption.class, new CommodityOption(), ImgUrl.class, new ImgUrl(), Location.class, new Location(), Role.class, new Role(), Seller.class, new Seller(), ShopOrder.class, new ShopOrder());


        ObjectMapper mapper = new ObjectMapper();
        File file = new File("D:\\objects.js");
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        for (Map.Entry<Class<? extends BaseEntity>, BaseEntity> entry : list.entrySet()) {
            writer.write("let " + entry.getKey().getSimpleName() + " = " + mapper.writeValueAsString(entry.getValue()));
            writer.newLine();
        }
        writer.flush();
        writer.close();
    }

    @Test
    void test54(){
        String url = "http://localhost:50000/api/search/ID";
        String url2= "http://localhost:50000/api/search?param=value";
        String url3= "http://localhost:50000/api/search?param=value";//post
        String id = url.substring(url.indexOf("api") + 4, url.lastIndexOf("?") != -1 ? url.indexOf("?") : url.length());
        System.out.println(id);
    }

    @Test
    void test55() throws JsonProcessingException {
        String json = "{\"roles\": {\"roleName\": \"ADMIN\"}}";
        ObjectMapper mapper = new ObjectMapper();
        Pair pair = mapper.readValue(json, Pair.class);
        System.out.println(pair);
    }

    @Test
    void test56() throws JsonProcessingException {
        Triples<String, String, Object> triples = new Triples<>("roles", "roleName", "ADMIN");
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(triples));
    }

    @Test
    void test57() throws JsonProcessingException {
//        Triples<String, String, Object> triples = new Triples<>("roles", "roleName", "ADMIN");
        Triples<String, String, Object> triples = new Triples<>("roles", "roleName", "SELLER");
        Pairs<String, Object> pairs = new Pairs<>("userId", "USER-2020-11111111");
        Pairs<String, Object> pairs2 = new Pairs<>("userName", "c");
        ArrayList<Pairs<String, Object>> list = new ArrayList<>();
        list.add(pairs);
        list.add(pairs2);
        ArrayList<Triples<String, String, Object>> arrayList = new ArrayList<>();
        arrayList.add(triples);
        TableInfo tableInfo = new TableInfo(new Pairs<>("userId", "123456"), list, arrayList);
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(tableInfo));
    }
}
