package edu.hubu.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hubu.web.commons.*;
import edu.hubu.web.dao.impl.IUserDao;
import edu.hubu.web.model.Buyer;
import edu.hubu.web.model.Location;
import edu.hubu.web.model.User;
import edu.hubu.web.service.DefaultService;
import kotlin.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

/**
 * @author moonlan
 * date 2020/11/28 19:11
 */
@SuppressWarnings(value = "unused")
@SpringBootTest
public class DefaultServiceTests {
    @Autowired
    private IUserDao userDao;

    @Autowired
    private BCryptPasswordEncoder encoder;

    private DefaultService<User, String> userService;
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private DefaultService<Location, String> locationService;

    @Autowired
    private EntityManager entityManager;

    @PostConstruct
    void before() {
        userService = new DefaultService<>(userDao, User.class, entityManager, "isEntityIsDeleted", "isEntityIsForbidden");
    }

    @Test
    @Transactional
    void testFindEntityById() throws JsonProcessingException {
        User entity = userService.findEntityById("USER-2020-11111111");
        System.out.println(mapper.writeValueAsString(entity));
    }

    @Test
    @Transactional
    void testGetEntity() throws JsonProcessingException {
        User userId = userService.getEntityByUniqueField("userId", "USER-2020-11111111");
        System.out.println(mapper.writeValueAsString(userId));
    }

    @Test
    @Transactional
    void testFindEntitiesPaged() throws JsonProcessingException {
        PageData<User> paged = userService.findEntitiesPaged(0, 5);
        Objects.requireNonNull(paged.getData()).forEach(System.out::println);
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testAddEntity() throws JsonProcessingException {
        User user = new User();
        //        user.setUserId("USER-2020-11111112");
        user.setUserName("moning");
        user.setUserEmail("1640655123@qq.com");
        user.setUserPassword(encoder.encode("123456"));
        user.setUserTelephoneNumber("1234567901");
        User baseUser = userService.addEntity(user);
        System.out.println(mapper.writeValueAsString(baseUser));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testUpdateEntityById() throws JsonProcessingException {
        User user = new User();
        user.setUserId("USER-2020-11111112");
        user.setUserName("moning");
        user.setUserEmail("1640655123@qq.com");
        user.setUserPassword(encoder.encode("123456"));
        user.setUserTelephoneNumber("12345679012");
        User baseUser = userService.updateEntityById("USER-2020-11111112", user);
        System.out.println(mapper.writeValueAsString(baseUser));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testDeleteEntityById() throws JsonProcessingException {
        boolean entityById = userService.deleteEntityById("USER-2020-11111113");
        System.out.println(mapper.writeValueAsString(entityById));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testLogicDeleteEntityById() throws JsonProcessingException {
        boolean entityById = userService.logicDeleteEntityById("USER-2020-11111112");
        System.out.println(mapper.writeValueAsString(entityById));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testForbidEntityById() throws JsonProcessingException {
        boolean entityById = userService.forbidEntityById("USER-2020-11111112");
        System.out.println(mapper.writeValueAsString(entityById));
    }

    @Test
    void testGetCount() throws JsonProcessingException {
        System.out.println(userService.getCount());
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testUpdateStatus() {
        boolean statusById = userService.updateEntityStatusById("USER-2020-11111112", EntityStatus.NOT_FORBIDDEN);
        System.out.println(statusById);
    }

    @Autowired
    DefaultService<Buyer, String> buyerService;

    @Test
    @Transactional
    @Rollback(value = false)
    void testAddBuyer() {
        Buyer buyer = new Buyer();
        buyer.setUserName("tes");
        buyer.setUserEmail("test@qq.com");
        buyer.setUserPassword(encoder.encode("123456"));
        buyer.setUserTelephoneNumber("test");
        System.out.println(buyerService.addEntity(buyer));
    }

    @Test
    @Transactional
    @Rollback(value = false)
    void testAddLocation() {
        User id = userService.findEntityById("USER-2020-11111111");
        Location location = locationService.findEntityById("LOCATION-2020-11111111");
        id.getLocation().add(location);
        User user = userService.updateEntityById("USER-2020-11111111", id);
        System.out.println(user);
    }

    @Test
    @Transactional
    void testFindEntitiesPagedByCol() {
        PageData<User> col = userService.findEntitiesPagedByCol(0, 5, List.of(new Pair<>("userId", "BUYER-2020-66666666")), user -> true);
        System.out.println(col);
    }

    @Test
    @Transactional
    void testFindEntitiesPagedByFK() {
        Triples<String, String, Object> triples = new Triples<>("roles", "roleName", "ADMIN");
        PageData<User> paged = userService.findEntitiesPagedByFK(0, 5, List.of(triples), user -> true);
        System.out.println(paged);
    }

    @Test
    @Transactional
    void testFindEntitiesPagedBy() {
        Triples<String, String, Object> triples = new Triples<>("roles", "roleName", "SELLER");
        Pairs<String, Object> pairs = new Pairs<>("userId", "USER-2020-11111111");
        Pairs<String, Object> pairs2 = new Pairs<>("userName", "c");
        TableInfo tableInfo = new TableInfo();
        tableInfo.setPkName(pairs);
//        tableInfo.getNormalColumns().add(pairs2);
//        tableInfo.getFkColumns().add(triples);
        PageData<User> paged = userService.getEntitiesPaged(0, 5, tableInfo, user -> true);
        System.out.println(paged);
    }

    @Test
    @Transactional
    void testAddRefType() {
        Boolean roles = userService.addRefType("user_roles", new Pairs<>("user_roles_user_id", "USER-2020-11111111"), new Pairs<>("user_roles_role_id", "ROLE-2020-33333333"));
        System.out.println(roles);
    }

    @Test
    @Transactional
    void testDelRefType() {
        Boolean roles = userService.delRefType("user_roles", new Pairs<>("user_roles_user_id", "USER-2020-11111111"), new Pairs<>("user_roles_role_id", "ROLE-2020-33333333"));
        System.out.println(roles);
    }
}
