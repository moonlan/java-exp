package edu.hubu.web;

import edu.hubu.web.model.Seller;
import edu.hubu.web.service.DefaultService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.SerializationUtils;

import javax.transaction.Transactional;

@SpringBootTest
public class SellerTest {

    @Autowired
    private DefaultService<Seller,String> service;

    @Test
    @Transactional
    void testAddSeller(){
        Seller seller = new Seller();
        seller.setSellerProfit(1);
        seller.setUserEmail("15464513@qq.com");
        seller.setUserName("fyw2");
        seller.setUserPassword("123");
        seller.setUserTelephoneNumber("13551479451");
        byte[] serialize = SerializationUtils.serialize(seller);
        Seller seller2 = (Seller) SerializationUtils.deserialize(serialize);
        System.out.println(seller2);
        Seller seller1 = service.addEntity(seller);

    }
}
