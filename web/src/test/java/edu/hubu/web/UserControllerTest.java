package edu.hubu.web;

import com.alibaba.fastjson.JSON;
import edu.hubu.web.model.User;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author waxijiang
 */
@SpringBootTest
@AutoConfigureMockMvc
//配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Transactional
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testFindOneById() throws Exception {
        String id = "BUYER-2020-66666666";
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.get("/user/search/" + id).header("token", "token"))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    public void testFindByLikePage() throws Exception {
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.post("/user/search_like").header("token", "token")
                        .param("currentPage", String.valueOf(0)).param("size", String.valueOf(8)))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void findAllPage() throws Exception {
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.get("/user/search").header("token", "token")
                        .param("currentPage", String.valueOf(0)))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void updateById() throws Exception {
        User user = new User();
        String userJson = JSON.toJSONString(user);
        String id = "USER-2020-11111111";
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.put("/user/" + id).header("token", "token")
                        .contentType(MediaType.APPLICATION_JSON).content(userJson))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void deleteById() throws Exception {
        String id = "BUYER-2020-66666666";
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + id).header("token", "token"))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void forbiddenById() throws Exception {
        String id = "BUYER-2020-66666666";
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.patch("/user/" + id).header("token", "token"))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void add() throws Exception {
        User user = new User();
        String id = "USER-2020-11111121";
        String email = "1115645625@qq.com";
        String phone = "123452455";
        String username = "waxijiang";
        user.setUserId(id);
        user.setUserName(username);
        user.setUserEmail(email);
        user.setUserPassword("11111111");
        user.setUserTelephoneNumber(phone);
        String userJson = JSON.toJSONString(user);
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.post("/user/").header("token", "token")
                        .contentType(MediaType.APPLICATION_JSON).content(userJson))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

    @Test
    void count() throws Exception {
        MvcResult result =
                mockMvc.perform(MockMvcRequestBuilders.get("/user/count").header("token", "token"))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.code", CoreMatchers.is(200)))
                        .andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }

}

